package com.nwri.notifications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class NotificationServiceApplication {
  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceApplication.class);

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(NotificationServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }

  @RequestMapping("/test")
  public String test() {
    LOGGER.info("Executing test.....");
    return "TEST!";
  }
}
