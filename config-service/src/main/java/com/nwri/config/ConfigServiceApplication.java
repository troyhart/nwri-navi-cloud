package com.nwri.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServiceApplication {

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(ConfigServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }
}
