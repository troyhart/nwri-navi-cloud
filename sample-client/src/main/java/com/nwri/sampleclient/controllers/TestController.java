package com.nwri.sampleclient.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

  @Value("${security.oauth2.resource.user-info-uri}")
  private String userInfoUri;
  private OAuth2RestTemplate oauth2RestTemplate;
  private OAuth2RestTemplate oauth2RestTemplateLB;
  private OAuth2ClientContext clientContext;

  @Autowired
  public TestController(@Qualifier("oauth2RestTemplate") OAuth2RestTemplate oauth2RestTemplate,
      @Qualifier("oauth2RestTemplateLB") OAuth2RestTemplate oauth2RestTemplateLB, OAuth2ClientContext clientContext) {
    this.oauth2RestTemplate = oauth2RestTemplate;
    this.oauth2RestTemplateLB = oauth2RestTemplateLB;
    this.clientContext = clientContext;
  }

  @RequestMapping("/test/users")
  public String testUsers(Model m) {
    m.addAttribute("accessToken", clientContext.getAccessToken());

    ResponseEntity<Object> response = oauth2RestTemplate.exchange(userInfoUri, HttpMethod.GET, null, Object.class);
    m.addAttribute("testUser", response.getBody());

    return "test";
  }

  @RequestMapping("/test/notifications")
  public String testNotifications(Model m) {
    m.addAttribute("accessToken", clientContext.getAccessToken());
    ResponseEntity<String> response =
        oauth2RestTemplateLB.exchange("http://notification-service/test", HttpMethod.GET, null, String.class);

    m.addAttribute("testNotification", response.getBody());

    return "test";
  }


  @RequestMapping("/test")
  public String test(Model m) {
    OAuth2AccessToken t = clientContext.getAccessToken();
    m.addAttribute("accessToken", t);

    return "test";
  }
}
