package com.nwri.sampleclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableEurekaClient
public class SampleClientApplication {

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(SampleClientApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }
}
