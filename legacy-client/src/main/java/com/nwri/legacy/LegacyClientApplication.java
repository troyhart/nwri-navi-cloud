package com.nwri.legacy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableOAuth2Sso
public class LegacyClientApplication extends WebSecurityConfigurerAdapter {
  private static final Logger LOGGER = LoggerFactory.getLogger(ResourceServerConfigurerAdapter.class);

  public static void main(String[] args) {
    SpringApplication.run(LegacyClientApplication.class, args);
  }
//
//  @Override
//  public void configure(HttpSecurity http) throws Exception {
//// @formatter:off
//    http
//      .authorizeRequests()/*.antMatchers("/login").permitAll()*/.anyRequest().authenticated()
//      .and().csrf().disable();
//// @formatter:on
//  }

  @RestController
  public static class TestRestController {

    @RequestMapping("/test")
    public String test() {
      LOGGER.info("LEGACY TEST!");
      return "LEGACY TEST";
    }
  }
}
