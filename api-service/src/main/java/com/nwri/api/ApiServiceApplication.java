package com.nwri.api;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.nwri.api.filters.pre.LoggingFilter;

@SpringBootApplication
@EnableAsync
@EnableEurekaClient
@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableZuulProxy
@EnableOAuth2Sso
public class ApiServiceApplication extends WebSecurityConfigurerAdapter {

  @Override
  public void configure(HttpSecurity http) throws Exception {
// @formatter:off
    http
      //.antMatcher("/**")
        .authorizeRequests()
          .antMatchers("/", "/login/**", "/webjars/**", "/hystrix*/**", "/proxy*/**", 
              "/routes", "/mappings", "/features", "/trace", 
              "/env/**", "/health", "/configprops").permitAll()
          .anyRequest().authenticated()
      .and().csrf().disable();
// @formatter:on
  }

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(ApiServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }


  @Bean
  public LoggingFilter simpleFilter() {
    return new LoggingFilter();
  }
}
