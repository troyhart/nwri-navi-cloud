package com.nwri.discovery;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryServiceApplication {

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(DiscoveryServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }
}
