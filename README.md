# NaviCloud

A prototype of some of the infrastructure required to build a secure and robust microservices platform.

## API Service

API service is basically a reverse proxy for all services in the system (except config service I think, or maybe not...I don't know.) Additionally, this implementation uses [Netflix Hystrix](https://github.com/Netflix/Hystrix) to wrap each service execution in a [circuit breaker](http://martinfowler.com/bliki/CircuitBreaker.html).

## Config Service

Config service implements a spring cloud configuration server, which enables each service in the system to securely externalize all configurations.

## Discovery Service

 Discovery service provides a [Netflix Eureka](https://github.com/Netflix/eureka) server to all the services in the system. This service will be available on `localhost` port `8761` (or rather, refer to [the actual configuration](discovery-service/src/main/resources/application.yml).)

Each service will register as a client. Below is an example spring boot application implementation and configuration:

* application implementation

```java
package com.nwri.special;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MySpecialServiceApplication {

  public static void main(String[] args) {
    // @formatter:off
    new SpringApplicationBuilder(MySpecialServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
    // @formatter:on
  }
}
```
[`@EnableEurekaClient` is the "implementation", and [ApplicationPidFileWriter](http://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/actuate/system/ApplicationPidFileWriter.html) is optional.]

* application configuration

```yml
spring:
  application:
    name: '@project.artifactId@'
eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/
```
[NOTE: The `eureka.client` configuration above will need some tuning, but this will serve our needs for a prototype.]

## User Service

Implements Authentication and Authorization. Provides an OAuth 2.0 Authorization Server. Integrates with OpenId (for active directory potentially???) and SAML for SSO.

## Preference Service

Preferences for some abstraction I can't think of yet. Whatever it is, it will be fulfilled by a user and/or a group, for starters.

## Notification Service

Notifications....like preferences, there's some modeling to do around the concept of a notification domain. It will certainly involve notifying users and administrators, but could be more general purpose potentially???
