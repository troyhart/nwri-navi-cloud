package com.nwri.users.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.saml.metadata.MetadataManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nwri.users.sterotypes.CurrentUser;

@Controller
@RequestMapping("/saml")
public class SSOController {

  // Logger
  private static final Logger LOG = LoggerFactory.getLogger(SSOController.class);

  @Autowired
  private MetadataManager metadata;

  @RequestMapping(value = "/idpSelection", method = RequestMethod.GET)
  public String idpSelection(HttpServletRequest request, Model model) {
    if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
      return "redirect:/ssoerror";
    }
    else {
      if (isForwarded(request)) {
        Set<String> idps = metadata.getIDPEntityNames();
        model.addAttribute("idps", idps);
        if (LOG.isDebugEnabled()) {
          LOG.debug("Federated IDPs: " + StringUtils.collectionToCommaDelimitedString(idps));
        }
        return "saml/idpselection";
      }
      else {
        LOG.warn("Direct accesses to '/idpSelection' route are not allowed");
        return "redirect:/ssoerror";
      }
    }
  }

  /* Checks if an HTTP request is forwarded from servlet. */
  private boolean isForwarded(HttpServletRequest request) {
    if (request.getAttribute("javax.servlet.forward.request_uri") == null) return false;
    else return true;
  }
}
