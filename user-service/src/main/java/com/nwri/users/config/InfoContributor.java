package com.nwri.users.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.stereotype.Component;

@Component
public class InfoContributor implements org.springframework.boot.actuate.info.InfoContributor {

  @Value("${spring.application.name}")
  private String serviceName;
  @Value("${spring.application.version}")
  private String serviceVersion;

  @Override
  public void contribute(Builder builder) {
    // @formatter:off
    builder
      .withDetail("service", serviceName+"-"+serviceVersion);
    // @formatter:on
  }

}
