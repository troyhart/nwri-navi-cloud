package com.nwri.users.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
  @Override
  public void configure(HttpSecurity http) throws Exception {
// @formatter:off
    http
      //.antMatcher("/**")
        .authorizeRequests().antMatchers("/resources/**", "/saml/**", "/login",
            "/mappings", "/actuator", "/features", "/trace", 
            "/env/**", "/health", "/configprops").permitAll()
          .anyRequest().authenticated()
      .and().csrf().disable();
// @formatter:on
  }

  @Bean
  HttpSessionSecurityContextRepository contextRepository() {
    return new HttpSessionSecurityContextRepository();
  }
}
