package com.nwri.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaAuditing
@EnableJpaRepositories
@RestController
public class UserServiceApplication {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceApplication.class);

  public static void main(String[] args) {
// @formatter:off
    new SpringApplicationBuilder(UserServiceApplication.class)
      .listeners(new ApplicationPidFileWriter())
      .build()
      .run(args);
// @formatter:on
  }

  @RequestMapping("/test")
  public String performTest() {
    LOGGER.info("Performing test........");
    return "testing testing 123";
  }
}

// TODO: here is a list of things that still need to be done here in user-service
// * Switch from InMemoryTokenStore to (probably) JdbcTokenStore (see /user-service/src/main/java/com/nwri/users/config/AuthorizationServerConfig.java)
//    - This will be needed in order to spin up multiple user-service instances.